declare module '*.scss';
declare module '*.json';
declare module '*.module.scss' {
  const content: { [className: string]: string };
  export = content;
}
