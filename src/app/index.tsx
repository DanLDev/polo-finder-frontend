import React from 'react';
import Classes from './App.module.scss';
import { createBrowserHistory } from 'history';
import PoloFinderProvider from 'modules/polo-finder/provider';
import PoloFinderPage from 'modules/polo-finder/containers/page';
export const history = createBrowserHistory();

const App = () => {
  return (
    <div className={Classes.App}>
      <PoloFinderProvider>
        <PoloFinderPage />
      </PoloFinderProvider>
    </div>
  );
};

export default App;
