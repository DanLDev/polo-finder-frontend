import React from 'react';
import Classes from './icon.module.scss';

export const ChevronUpIcon = () => {
  return (
    <svg
      className={Classes.SvgIcon}
      height="100%"
      width="100%"
      viewBox="0 0 40 40"
      xmlns="http://www.w3.org/2000/svg"
      transform={'rotate(180)'}
    >
      <g height="100%" transform={`translate(0,0)`}>
        <line x1="10" y1="16" x2="20" y2="24" stroke="currentColor" strokeLinecap="round" />
        <line x1="20" y1="24" x2="30" y2="16" stroke="currentColor" strokeLinecap="round" />
      </g>
    </svg>
  );
};
