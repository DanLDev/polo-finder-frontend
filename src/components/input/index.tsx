import React from 'react';
import Classes from './input.module.scss';

export const Input = ({ value, onChange = value => {}, type = 'text' }) => {
  return (
    <input
      className={Classes.Input}
      type={type}
      value={value}
      onChange={e => onChange(e.target.value)}
    />
  );
};
