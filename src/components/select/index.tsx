import React, { useEffect, useState, useRef } from "react";
import Classes from "./select.module.scss";
import { useClickOutside } from "hooks/";
import { ChevronUpIcon } from "components/icon/chevronUp";
import { ChevronDownIcon } from "components/icon/chevronDown";
export const Select = ({ options, value = null, onChange = (value) => {} }) => {
  const [isOpen, setIsOpen] = useState(false);
  const selectRef = useRef(null);
  const selectedOption = options.find((option) => option.value === value);

  useEffect(() => {
    if (!selectedOption) {
      onChange(options[0].value);
    }
  }, [value]);

  useClickOutside(selectRef, () => {
    setIsOpen(false);
  });

  return (
    <div className={Classes.Select} ref={(el) => (selectRef.current = el)}>
      <div
        className={Classes.Select_selected}
        onClick={() => setIsOpen(!isOpen)}
      >
        <div className={Classes.Select_selected_icon}></div>
        {selectedOption?.label}
        <div className={Classes.Select_selected_chevron}>
          {isOpen ? <ChevronUpIcon /> : <ChevronDownIcon />}
        </div>
      </div>
      {isOpen && (
        <div className={Classes.Select_options}>
          {options.map((option, key) => (
            <div
              className={Classes.Select_option}
              key={key}
              onClick={() => {
                onChange(option.value);
                setIsOpen(false);
              }}
            >
              <div className={Classes.Select_option_icon} />
              {option.label}
            </div>
          ))}
        </div>
      )}
    </div>
  );
};
