import React from 'react';
import { ButtonProps } from './interfaces';
import Classes from './button.module.scss';

export const Button = (props: ButtonProps) => {
  const styles = {
    primary: Classes.Button___primary,
    secondary: Classes.Button___secondary,
  };
  return (
    <button
      onClick={e => {
        if (props.stopPropagation) {
          e.stopPropagation();
        }
        props.action();
      }}
      className={`${Classes.Button} ${styles[props.style]} ${
        props.disabled && Classes.Button___disabled
      }`}
      disabled={props.disabled}
    >
      {props?.text}
    </button>
  );
};
