export interface ButtonProps {
  action: () => void;
  text?: string;
  style: 'primary' | 'secondary';
  stopPropagation?: boolean;
  disabled?: boolean;
}
