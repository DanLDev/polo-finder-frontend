import React from 'react';
import ReactDOM from 'react-dom';
import App from 'src/app/index';
import 'styles/core/index.scss';

ReactDOM.render(<App />, document.getElementById('root'));
