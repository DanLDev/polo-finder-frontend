import { createContext, useContext } from 'react';

export const PoloFinderContext = createContext(null);

export function usePoloFinder() {
  return useContext(PoloFinderContext);
}
