import React, { useReducer, useMemo } from 'react';
import { PoloFinderContext } from './context';
import appProviderReducer from './reducer';
import { PoloProviderState } from './interfaces';
import {
  SET_PROGRESS,
  SET_NEXT_QUESTION,
  SET_QUESTION_PICKER,
  SET_SEARCH_UNSUCCESSFUL,
  SET_FOUND_CAR,
} from './reducer/types';
import { QuestionPicker } from './question-picker';
import cars from './cars.json';

const initialState: PoloProviderState = {
  progress: 0,
  searchUnsucessful: false,
};

const PoloFinderProvider = props => {
  const [state, dispatch] = useReducer(appProviderReducer, initialState);

  const initialiseQuestions = () => {
    const qp = new QuestionPicker(cars);
    const nextQuestion = qp.getNextQuestion();
    dispatch({ type: SET_QUESTION_PICKER, payload: qp });
    dispatch({ type: SET_NEXT_QUESTION, payload: nextQuestion });
  };

  const submitAnswer = (answer): void => {
    const qp = state.questionPicker;
    const result = qp.submitAnswer(answer);
    dispatch({ type: SET_PROGRESS, payload: result.progress });

    if (result.found) {
      dispatch({ type: SET_FOUND_CAR, payload: result.car });
    } else if (result.progress < 100) {
      getNextQuestion();
    } else {
      dispatch({ type: SET_SEARCH_UNSUCCESSFUL, payload: true });
    }
  };

  const getNextQuestion = () => {
    const qp = state.questionPicker;
    const nextQuestion = qp.getNextQuestion();

    if (nextQuestion !== undefined) {
      dispatch({ type: SET_NEXT_QUESTION, payload: nextQuestion });
    } else {
      dispatch({ type: SET_SEARCH_UNSUCCESSFUL, payload: true });
    }
  };

  const skipQuestion = () => {
    getNextQuestion();
  };

  const reset = () => {
    const qp = state.questionPicker;
    qp.reset();
    const nextQuestion = qp.getNextQuestion();
    dispatch({ type: SET_FOUND_CAR, payload: null });
    dispatch({ type: SET_PROGRESS, payload: 0 });
    dispatch({ type: SET_NEXT_QUESTION, payload: nextQuestion });
    dispatch({ type: SET_SEARCH_UNSUCCESSFUL, payload: false });
  };

  const value = useMemo(
    () => ({
      progress: state.progress,
      nextQuestion: state.nextQuestion,
      isInitialised: state.questionPicker !== undefined,
      searchUnsuccessful: state.searchUnsucessful,
      foundCar: state.foundCar,
      skipQuestion,
      submitAnswer,
      initialiseQuestions,
      reset,
    }),
    [
      state.foundCar,
      state.questionPicker,
      state.progress,
      state.nextQuestion,
      state.searchUnsucessful,
    ],
  );

  return <PoloFinderContext.Provider value={value} {...props} />;
};

export default PoloFinderProvider;
