import { car, question } from "./interfaces";

export const questions: question[] = [
  {
    text: "What type of transmission does the car have?",
    options: [
      {
        label: "Automatic",
        value: "A",
      },
      {
        label: "Manual",
        value: "M",
      },
    ],
    getValue: (car: car) => car.model.transmission,
    assertion: (transmission: string, car: car) =>
      transmission === car.model.transmission,
  },
  {
    text: "What type of fuel does the car take?",
    options: [
      {
        label: "Petrol",
        value: "P",
      },
      {
        label: "Diesel",
        value: "D",
      },
    ],
    getValue: (car: car) => car.model.fuelType,
    assertion: (fuel: string, car: car) => fuel === car.model.fuelType,
  },
  {
    text: "What size engine does the car have?",
    options: [
      {
        label: "0cc to 999cc",
        value: [0, 999],
      },
      {
        label: "1L to 1099cc",
        value: [1000, 1099],
      },
      {
        label: "1.1L to 1199cc",
        value: [1100, 1199],
      },
      {
        label: "1.2L to 1299cc",
        value: [1200, 1299],
      },
      {
        label: "1.3L to 1399cc",
        value: [1300, 1399],
      },
      {
        label: "1.4L to 1499cc",
        value: [1400, 1499],
      },
      {
        label: "1.5L to 1599cc",
        value: [1500, 1599],
      },
      {
        label: "1.6L to 1699cc",
        value: [1600, 1699],
      },
      {
        label: "1.7L to 1799cc",
        value: [1700, 1799],
      },
      {
        label: "1.8L or more",
        value: [1800, 4000],
      },
    ],
    getValue: (car: car) => car.model.cc,
    assertion: (engineSizeRange: number[], car: car) =>
      engineSizeRange[0] <= car.model.cc && engineSizeRange[1] >= car.model.cc,
  },
  {
    text: "How Many doors does it have?",
    options: [
      {
        label: "2 Doors",
        value: 2,
      },
      {
        label: "3 Doors",
        value: 3,
      },
      {
        label: "4 Doors",
        value: 4,
      },
      {
        label: "5 Doors",
        value: 5,
      },
      {
        label: "6 Doors",
        value: 6,
      },
    ],
    getValue: (car: car) => car.model.doorCount,
    assertion: (doorCount: number, car: car) =>
      doorCount === car.model.doorCount,
  },
  {
    text: "Is your car model in this list?",
    constructOptions: (cars: car[]) =>
      cars.reduce((accumulator, car: car) => {
        if (!accumulator.find((option) => option.value === car.model.model)) {
          accumulator.push({ label: car.model.model, value: car.model.model });
        }
        return accumulator;
      }, []),
    getValue: (car: car) => car.model.model,
    assertion: (model: string, car: car) => car.model.model === model,
  },
  {
    text: "When was the car manufactured?",
    getValue: (car: car) => [car.model.yearFrom, car.model.yearTo],
    constructOptions: (cars: car[]) => {
      const possibleYears = cars.reduce((accumulator, car) => {
        const yearFrom = parseInt(car.model.yearFrom);
        const yearTo = parseInt(car.model.yearTo);
        const years = Array.from(
          { length: yearTo - yearFrom },
          (v, i) => i + yearFrom
        );
        years.forEach((year) => {
          if (!accumulator.find((value) => value === year)) {
            accumulator.push(year);
          }
        });

        return accumulator;
      }, []);

      return possibleYears.map((year) => ({ label: year, value: year }));
    },
    assertion: (manufactureYear: number, car: car) =>
      manufactureYear >= parseInt(car.model.yearFrom) &&
      manufactureYear <= parseInt(car.model.yearTo),
  },
  {
    text: "What series is the car?",
    getValue: (car: car) => car.model.series,
    constructOptions: (cars: car[]) =>
      cars.reduce((accumulator, car) => {
        const series = car.model.series;
        if (!accumulator.find((option) => option.value === series)) {
          accumulator.push({
            label: series !== "" ? series : "Other",
            value: series,
          });
        }
        return accumulator;
      }, []),
    assertion: (series: string, car: car) => series === car.model.series,
  },
  {
    text: "How powerful is it?",
    getValue: (car: car) => car.details.bhpCount,
    constructOptions: (cars: car[]) =>
      cars.reduce((accumulator, car: car) => {
        const power = car.details.bhpCount;
        if (!accumulator.find((option) => option.value === power)) {
          accumulator.push({
            label: power !== null ? power + " Bhp" : "Not Listed",
            value: power,
          });
        }
        return accumulator;
      }, []),
    assertion: (power: number, car: car) => power === car.details.bhpCount,
  },
  {
    text: "Security group status",
    getValue: (car: car) => car.security.groupStatus,
    constructOptions: (cars: car[]) =>
      cars.reduce((accumulator, car: car) => {
        const groupStatus = car.security.groupStatus;
        if (!accumulator.find((option) => option.value === groupStatus)) {
          accumulator.push({
            label: groupStatus !== "" ? groupStatus : "Other",
            value: groupStatus,
          });
        }
        return accumulator;
      }, []),
    assertion: (groupStatus: string, car: car) =>
      groupStatus === car.security.groupStatus,
  },
  {
    text: "Kerb Weight",
    getValue: (car: car) => car.details.kerbWeight,
    constructOptions: (cars: car[]) =>
      cars.reduce((accumulator, car: car) => {
        const kerbWeight = car.details.kerbWeight;
        if (!accumulator.find((option) => option.value === kerbWeight)) {
          accumulator.push({
            label: kerbWeight > 0 ? kerbWeight + " Kg" : "Other",
            value: kerbWeight,
          });
        }
        return accumulator;
      }, []),
    assertion: (weight: number, car: car) => weight === car.details.kerbWeight,
  },
];
