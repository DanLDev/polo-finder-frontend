import { question, questionPicker, car } from './index';

export interface PoloProviderState {
  progress: number;
  nextQuestion?: question;
  questionPicker?: questionPicker;
  searchUnsucessful?: boolean;
  foundCar?: car;
}
