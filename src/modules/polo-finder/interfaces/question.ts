import { car } from "./index";

export interface question {
  text: string;
  assertion: (answer: any, car: car) => boolean;
  getValue: (car) => any;
  constructOptions?: (cars: car[]) => { label: string; value: any }[];
  options?: { label: string; value: any }[];
}
