export interface car {
  model: model;
  keeper: any;
  details: details;
  security: security;
  valuations: any;
}

interface model {
  make: string;
  model: string;
  baseModel: string;
  series: string;
  cc: number;
  yearFrom: string;
  yearTo: string;
  doorCount: number;
  seatCount: number;
  fuelType: "P" | "D";
  transmission: "A" | "M";
  abiCode: string;
  type?: string;
  cdlCode?: string;
  co2?: string;
}

interface details {
  manufacturedDate: any;
  registrationDate: any;
  vinEnding: any;
  colour: any;
  registration: any;
  bodyDesc: string;
  bodyClass: string;
  importedUk: any;
  grossWeight: number;
  kerbWeight: number;
  bhpCount: number | null;
}

interface security {
  groupRatings: { range: string; rating: string }[];
  groupStatus: string;
  groupStatusDescription: any;
  securityStatus: string;
  securityStatusDescription: any;
}
