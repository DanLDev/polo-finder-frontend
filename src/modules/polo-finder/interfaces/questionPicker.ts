import { question, car } from './index';

export interface questionPicker {
  submitAnswer: (answer: any) => { progress?: number; found?: boolean; car?: car };
  getNextQuestion: () => question;
}
