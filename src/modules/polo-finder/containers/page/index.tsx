import React from 'react';
import Classes from './page.module.scss';
import { PoloFinder } from '../../components/polo-finder';

const PoloFinderPage = () => {
  return (
    <div className={Classes.PoloFinderPage}>
      <div className={Classes.PoloFinderPage_header}>
        <h1> Polo Finder</h1>
      </div>
      <div className={Classes.PoloFinderPage_content}>
        <PoloFinder />
      </div>
    </div>
  );
};

export default PoloFinderPage;
