import { car, question, questionPicker } from "./interfaces";
import { questions } from "./questions";

export class QuestionPicker {
  cars: car[];
  questions: question[];
  questionNumber: number;
  currentQuestion: question;
  initialCars: car[];

  constructor(cars) {
    this.initialCars = cars;
    this.cars = cars;
    this.questions = [...questions];

    this.getNextQuestion.bind(this);
    this.submitAnswer.bind(this);
    this.pickQuestion.bind(this);
    this.removeQuestion.bind(this);
    this.reset.bind(this);
  }

  getNextQuestion(): question {
    // Get a the next question
    this.currentQuestion = this.pickQuestion();

    return this.currentQuestion;
  }

  // Loop through remaining questions to
  // find which is most likely to narrow down the selection
  pickQuestion(): question {
    let chosenQuestionWithOptions;
    let chosenQuestion;
    let maxDivergence = 0;

    this.questions.forEach((question) => {
      const builtQuestion = this.buildQuestion(question, this.cars);
      const divergence = this.calculateDivergence(builtQuestion, this.cars);

      // Prevent questions with too many options
      if (builtQuestion?.options?.length > 10) {
        return;
      }

      if (divergence > 1 && divergence > maxDivergence) {
        chosenQuestionWithOptions = builtQuestion;
        chosenQuestion = question;
        maxDivergence = divergence;
      } else if (divergence < 2) {
        // if this question produces no divergence, remove it from the remaining questions
        this.removeQuestion(question);
      }
    });

    this.removeQuestion(chosenQuestion);
    return chosenQuestionWithOptions;
  }

  // Remove the question from the remaining questions
  removeQuestion(question: question) {
    this.questions = this.questions.filter(
      (remainingQuestion) => remainingQuestion !== question
    );
  }

  // Calculate the divergence in possible options between the remaining cars
  calculateDivergence(question: question, cars: car[]): number {
    const options = cars.reduce((accumulator, car) => {
      const optionValue = question.getValue(car);
      if (!accumulator.includes(optionValue)) {
        accumulator.push(optionValue);
      }
      return accumulator;
    }, []);
    return options.length;
  }

  // Build question options based on remaining cars, if an option will return 0 results, remove it
  buildQuestion(question: question, cars: car[]): question {
    let options = [];
    if (!question.options) {
      options = question.constructOptions(cars);
    } else {
      question.options.forEach((option) => {
        const possibleResults = cars.filter((car) =>
          question.assertion(option.value, car)
        );

        if (possibleResults.length) {
          options.push(option);
        }
      });
    }
    return { ...question, options: options };
  }

  // filter cars based on answer.
  submitAnswer(answer) {
    const filteredCars = this.cars.filter((car) =>
      this.currentQuestion.assertion(answer, car)
    );

    // If we ran out of cars
    // (Should never happen since we wont give any options which result in 0 cars)
    if (!filteredCars.length) {
      return {
        found: false,
        progress: 100,
      };
    }

    // If we have 1 left, we found it!
    if (filteredCars.length === 1) {
      return {
        found: true,
        progress: 100,
        car: filteredCars[0],
      };
    }

    // If theres still cars left, keep going
    if (filteredCars.length > 1) {
      const percentageRemaining =
        (filteredCars.length / this.initialCars.length) * 100;
      this.cars = filteredCars;
      return {
        found: false,
        progress: 100 - percentageRemaining,
      };
    }
  }

  reset() {
    this.cars = this.initialCars;
    this.questions = [...questions];
  }
}
