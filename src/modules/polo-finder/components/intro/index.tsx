import React from 'react';
import { usePoloFinder } from '../../context';
import { Button } from 'components/button';
import Classes from './intro.module.scss';

export const Intro = () => {
  const { isInitialised, initialiseQuestions } = usePoloFinder();

  return (
    <>
      {!isInitialised && (
        <div className={Classes.Intro}>
          <h2>Find your Polo ABI code</h2>
          <Button
            style={'primary'}
            action={() => {
              initialiseQuestions();
            }}
            text={'Get Started'}
          />
        </div>
      )}
    </>
  );
};
