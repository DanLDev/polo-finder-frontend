import React from 'react';
import { usePoloFinder } from '../../context';
import Classes from './progress-bar.module.scss';

export const ProgressBar = () => {
  const { progress } = usePoloFinder();

  return (
    <div className={Classes.ProgressBar}>
      <div className={Classes.ProgressBar_inner} style={{ width: `${progress}%` }} />
    </div>
  );
};
