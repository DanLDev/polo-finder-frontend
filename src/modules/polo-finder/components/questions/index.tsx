import React, { useEffect, useState } from "react";
import { usePoloFinder } from "../../context";
import Classes from "./questions.module.scss";
import { Select } from "components/select";

import { Button } from "components/button";
export const Questions = () => {
  const { nextQuestion, submitAnswer, skipQuestion } = usePoloFinder();
  const [answer, setAnswer] = useState(null);

  useEffect(() => {
    setAnswer(undefined);
  }, [nextQuestion]);
  return (
    <>
      {nextQuestion && (
        <div className={Classes.Questions}>
          <h2 className={Classes.Questions_text}>{nextQuestion.text}</h2>

          <div className={Classes.Questions_input}>
            <Select
              options={nextQuestion.options}
              onChange={(value) => setAnswer(value)}
              value={answer}
            />
          </div>

          <div className={Classes.Questions_actions}>
            <div className={Classes.Questions_skip}>
              <Button
                style={"secondary"}
                action={() => skipQuestion()}
                text={"Skip Question"}
              />
            </div>
            <div className={Classes.Questions_submit}>
              <Button
                disabled={answer === undefined}
                style={"primary"}
                action={() => submitAnswer(answer)}
                text={"Next question"}
              />
            </div>
          </div>
        </div>
      )}
    </>
  );
};
