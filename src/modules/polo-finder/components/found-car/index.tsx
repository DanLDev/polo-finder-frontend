import React from 'react';
import axios from 'axios';
import { usePoloFinder } from '../../context';
import Classes from './found-car.module.scss';
import { Button } from 'components/button';
export const FoundCar = () => {
  const { foundCar, reset } = usePoloFinder();

  const getFact = async () => {
    try {
      const factNumber = foundCar.model.abiCode.slice(0, 3);
      const fact = (await axios.get(`http://numbersapi.com/${factNumber}`)).data;
      window.alert(fact);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className={Classes.FoundCar}>
      <h2>We found your Polo!</h2>
      <p>Your ABI Code: {foundCar?.model?.abiCode}</p>
      <div className={Classes.FoundCar_actions}>
        <Button text={'Tell me a fact'} style={'primary'} action={() => getFact()} />
        <Button text={'Start again'} style={'secondary'} action={() => reset()} />
      </div>
    </div>
  );
};
