import React from 'react';
import { ProgressBar } from '../../components/progress-bar';
import { Questions } from '../questions';
import { usePoloFinder } from '../../context';
import { Intro } from '../intro';
import { TryAgain } from '../try-again';
import { FoundCar } from '../found-car';
import Classes from './polo-finder.module.scss';
export const PoloFinder = () => {
  const { nextQuestion, searchUnsuccessful, foundCar } = usePoloFinder();

  return (
    <div className={Classes.PoloFinder}>
      <div className={Classes.PoloFinder_progress}>
        <ProgressBar />
      </div>
      <div className={Classes.PoloFinder_content}>
        <Intro />
        {!searchUnsuccessful && !foundCar && <Questions />}
        {searchUnsuccessful && <TryAgain />}
        {foundCar && <FoundCar />}
      </div>
    </div>
  );
};
