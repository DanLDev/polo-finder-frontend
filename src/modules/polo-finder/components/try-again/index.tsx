import React from 'react';
import { Button } from 'components/button';
import { usePoloFinder } from '../../context';
import Classes from './try-again.module.scss';

export const TryAgain = () => {
  const { reset } = usePoloFinder();
  return (
    <div className={Classes.TryAgain}>
      <h2>Sorry, We weren't able to find an exact match from the responses you gave us</h2>
      <Button style={'secondary'} text={'Try again'} action={() => reset()} />
    </div>
  );
};
