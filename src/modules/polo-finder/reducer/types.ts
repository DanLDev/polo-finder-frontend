export const SET_PROGRESS = 'SET_PROGRESS';
export const SET_NEXT_QUESTION = 'SET_NEXT_QUESTION';
export const SET_QUESTION_PICKER = 'SET_QUESTION_PICKER';
export const SET_SEARCH_UNSUCCESSFUL = 'SET_SEARCH_UNSUCCESSFUL';
export const SET_FOUND_CAR = 'SET_FOUND_CAR';
