import {
  SET_PROGRESS,
  SET_NEXT_QUESTION,
  SET_QUESTION_PICKER,
  SET_FOUND_CAR,
  SET_SEARCH_UNSUCCESSFUL,
} from './types';
import { PoloProviderState } from '../interfaces';
import produce from 'immer';

const handlers = {
  [SET_PROGRESS]: (draft: PoloProviderState, { payload }) => {
    draft.progress = payload;
  },
  [SET_NEXT_QUESTION]: (draft: PoloProviderState, { payload }) => {
    draft.nextQuestion = payload;
  },
  [SET_QUESTION_PICKER]: (draft: PoloProviderState, { payload }) => {
    draft.questionPicker = payload;
  },
  [SET_SEARCH_UNSUCCESSFUL]: (draft: PoloProviderState, { payload }) => {
    draft.searchUnsucessful = payload;
  },
  [SET_FOUND_CAR]: (draft: PoloProviderState, { payload }) => {
    draft.foundCar = payload;
  },
};

const PoloFinderProviderReducer = produce((draft, action) => handlers[action.type](draft, action));

export default PoloFinderProviderReducer;
